{
  lib,
  buildPythonPackage,
  requests,
  # flake
  self,
}: let
  inherit (lib.sources) cleanSource;
in
  buildPythonPackage {
    pname = "python-bgpview";
    version = "0.1.0";

    src = cleanSource self;

    propagatedBuildInputs = [requests];

    pythonImportsCheck = ["bgpview"];
  }
