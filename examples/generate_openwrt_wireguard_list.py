from netaddr import IPNetwork

import time
from datetime import datetime
import bgpview
import netaddr

subsystem = "network.@wireguard_wgscrumplex[0].allowed_ips"
target_asns = [13335]  # CloudFlare
additional_ips = [
    "10.255.255.0/24",
    "fd69:5efa:5efa:5efa::0/64",
]  # extra IPs for the WG interface


def print_openwrt_wg(subnets):
    print(f"uci delete {subsystem}")
    for net in subnets:
        print(f"uci add_list {subsystem}='{str(net)}'")


api_start = time.time()

bgp = bgpview.BGPView()

v4 = []
v6 = []

for asn in target_asns:
    p = bgp.get_asn_prefixes(asn)
    v4 += list(map(lambda y: IPNetwork(y["prefix"]), p.ipv4_prefixes))
    v6 += list(map(lambda y: IPNetwork(y["prefix"]), p.ipv6_prefixes))

api_end = time.time()

merged = sorted(netaddr.cidr_merge(v4) + netaddr.cidr_merge(v6))

merge_end = time.time()

print_openwrt_wg(merged + additional_ips)
print()
print(f"# Total Prefixes: {len(v4) + len(v6)}, Merged Prefixes: {len(merged)}")
print(
    f"# Total API time: {api_end - api_start}s, Total merge time: {merge_end - api_end}s"
)
print(f"# Data from {datetime.isoformat(datetime.now())}")
