from netaddr import IPNetwork

import bgpview
import netaddr


def print_openwrt_wg(subnets):
    for net in subnets:
        print(f"\tlist allowed_ips '{str(net)}'")


bgp = bgpview.BGPView()

p = bgp.get_asn_prefixes(3320)  # AS3320 -> DTAG

v4 = list(map(lambda y: IPNetwork(y["prefix"]), p.ipv4_prefixes))
v6 = list(map(lambda y: IPNetwork(y["prefix"]), p.ipv6_prefixes))

merged = sorted(netaddr.cidr_merge(v4) + netaddr.cidr_merge(v6))

print_openwrt_wg(merged)
